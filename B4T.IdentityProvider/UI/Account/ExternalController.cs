using IdentityModel;
using B4T.IdentityProvider.Constants;
using B4T.IdentityProvider.Models;
using IdentityServer4.Events;
using IdentityServer4.Services;
using IdentityServer4.Stores;

using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using B4T.IdentityProvider.Extensions;
using IdentityServer4;

namespace B4T.IdentityProvider.UI
{
    [SecurityHeaders]
    [AllowAnonymous]
    public class ExternalController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IIdentityServerInteractionService _interaction;
        private readonly IClientStore _clientStore;        
        private readonly IEventService _events;
        private readonly ILogger<ExternalController> _logger;

        public ExternalController(
            UserManager<ApplicationUser> userManager,
            IIdentityServerInteractionService interaction,
            IClientStore clientStore,
            IEventService events,
            ILogger<ExternalController> logger
            )
        {
            _userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
            _interaction = interaction ?? throw new ArgumentNullException(nameof(interaction));
            _clientStore = clientStore ?? throw new ArgumentNullException(nameof(clientStore));
            _events = events ?? throw new ArgumentNullException(nameof(events));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        /// <summary>
        /// initiate roundtrip to external authentication provider
        /// </summary>
        [HttpGet]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1054:Uri parameters should not be strings", Justification = "Path string expected.")]
        public async Task<IActionResult> Challenge(string provider, string returnUrl)
        {
            if (string.IsNullOrEmpty(returnUrl)) returnUrl = "~/";

            // validate returnUrl - either it is a valid OIDC URL or back to a local page
            if (Url.IsLocalUrl(returnUrl) == false && _interaction.IsValidReturnUrl(returnUrl) == false)
            {
                // user might have clicked on a malicious link
                _logger.LogWarning("Throw Exception: User might have clicked on a malicious link {@maliciousLink}", returnUrl);
                throw new Exception("invalid return URL");
            }

            if (AccountOptions.WindowsAuthenticationSchemeName == provider)
            {
                // windows authentication needs special handling
                return await ProcessWindowsLoginAsync(returnUrl).ConfigureAwait(false);
            }
            else
            {
                // start challenge and roundtrip the return URL and scheme 
                var props = new AuthenticationProperties
                {
                    RedirectUri = Url.Action(nameof(Callback)),
                    Items =
                    {
                        { "returnUrl", returnUrl },
                        { "scheme", provider },
                    }
                };

                return Challenge(props, provider);
            }
        }

        /// <summary>
        /// Post processing of external authentication
        /// </summary>
        [HttpGet]
        public async Task<IActionResult> Callback()
        {
            // read external identity from the temporary cookie
            var result = await HttpContext.AuthenticateAsync(IdentityServer4.IdentityServerConstants.ExternalCookieAuthenticationScheme).ConfigureAwait(false);
            if (result?.Succeeded != true)
            {
                _logger.LogWarning("{WarnMsg}", "External authentication failed. Throw Exception");
                throw new Exception("External authentication failed");
            }

            if (_logger.IsEnabled(LogLevel.Debug))
            {
                var externalClaims = result.Principal.Claims.Select(c => $"{c.Type}: {c.Value}");
                _logger.LogDebug("External claims: {@claims}", externalClaims);
            }

            // lookup our user and external provider info
            var (user, provider, providerUserId, claims) = await FindUserFromExternalProvider(result).ConfigureAwait(false);

            var userEmail = claims.FirstOrDefault(x => x.Type == JwtClaimTypes.Email)?.Value ??
               claims.FirstOrDefault(x => x.Type == ClaimTypes.Email)?.Value ?? "Email not found";

            // for new user with email end with "book4time.com" only, provision a new user
            if (user == null && userEmail.EndsWith("@book4time.com", StringComparison.InvariantCultureIgnoreCase))
            {
                user = await AutoProvisionUser(provider, providerUserId, claims).ConfigureAwait(false);
            }

            // this allows us to collect any additonal claims or properties
            // for the specific prtotocols used and store them in the local auth cookie.
            // this is typically used to store data needed for signout from those protocols.
            var additionalLocalClaims = new List<Claim>();
            var localSignInProps = new AuthenticationProperties();
            ProcessLoginCallbackForOidc(result, additionalLocalClaims, localSignInProps);
            /* Not use template method
            ProcessLoginCallbackForWsFed(result, additionalLocalClaims, localSignInProps);
            ProcessLoginCallbackForSaml2p(result, additionalLocalClaims, localSignInProps);
            */

            // include the claims from external (external claims not store locally)            
            additionalLocalClaims.AddRange(ConvertToJwtClaims(claims));

            var userSubject = string.Empty;          
            if (user == null)
            {
                _logger.LogWarning("{WarnMsg}", "External user not found in local system.  Return external provider id");
                // external user not in our system.  Use information from external provider
                userSubject = providerUserId;                            
            }
            else
            {
                // Use internal user id as subject
                userSubject = user.Id;

                #region Add Book4Time specific user claims
                var userClaims = await _userManager.GetClaimsAsync(user).ConfigureAwait(false);
                additionalLocalClaims.AddRange(userClaims);

                // Add active claim if user is not lock out
                var isLockedOut = await _userManager.IsLockedOutAsync(user).ConfigureAwait(false);
                if (!isLockedOut) {
                    additionalLocalClaims.Add(new Claim(B4TClaim.ClaimTypeUserActive, "true", ClaimValueTypes.Boolean));
                }
                #endregion
            }

            // Use Name provided by external provider.  Use userEmail if name is not found
            var userName = claims.FirstOrDefault(x => x.Type == JwtClaimTypes.Name)?.Value ??
                        claims.FirstOrDefault(x => x.Type == ClaimTypes.Name)?.Value ??
                        userEmail;

            // issue authentication cookie for user
            var isuser = new IdentityServerUser(userSubject)
            {
                DisplayName = userName,
                IdentityProvider = provider,
                AdditionalClaims = additionalLocalClaims
            };

            await HttpContext.SignInAsync(isuser, localSignInProps).ConfigureAwait(false);


            // delete temporary cookie used during external authentication
            await HttpContext.SignOutAsync(IdentityServer4.IdentityServerConstants.ExternalCookieAuthenticationScheme).ConfigureAwait(false);

            // retrieve return URL
            var returnUrl = result.Properties.Items["returnUrl"] ?? "~/";

            // check if external login is in the context of an OIDC request
            var context = await _interaction.GetAuthorizationContextAsync(returnUrl).ConfigureAwait(false);            
            await _events.RaiseAsync(new UserLoginSuccessEvent(provider, providerUserId, userSubject, userName, true, context?.ClientId)).ConfigureAwait(false);

            if (context != null)
            {
                if (await _clientStore.IsPkceClientAsync(context.ClientId).ConfigureAwait(false))
                {
                    // if the client is PKCE then we assume it's native, so this change in how to
                    // return the response is for better UX for the end user.
                    return View("Redirect", new RedirectViewModel { RedirectUrl = returnUrl });
                }
            }

            return Redirect(returnUrl);
        }

        private async Task<IActionResult> ProcessWindowsLoginAsync(string returnUrl)
        {
            // see if windows auth has already been requested and succeeded
            var result = await HttpContext.AuthenticateAsync(AccountOptions.WindowsAuthenticationSchemeName).ConfigureAwait(false);
            if (result?.Principal is WindowsPrincipal wp)
            {
                // we will issue the external cookie and then redirect the
                // user back to the external callback, in essence, treating windows
                // auth the same as any other external authentication mechanism
                var props = new AuthenticationProperties()
                {
                    RedirectUri = Url.Action("Callback"),
                    Items =
                    {
                        { "returnUrl", returnUrl },
                        { "scheme", AccountOptions.WindowsAuthenticationSchemeName },
                    }
                };

                var id = new ClaimsIdentity(AccountOptions.WindowsAuthenticationSchemeName);
                id.AddClaim(new Claim(JwtClaimTypes.Subject, wp.FindFirst(ClaimTypes.PrimarySid).Value));
                id.AddClaim(new Claim(JwtClaimTypes.Name, wp.Identity.Name));

                /*
                // add the groups as claims -- be careful if the number of groups is too large
                if (AccountOptions.IncludeWindowsGroups)
                {
                    var wi = wp.Identity as WindowsIdentity;
                    var groups = wi.Groups.Translate(typeof(NTAccount));
                    var roles = groups.Select(x => new Claim(JwtClaimTypes.Role, x.Value));
                    id.AddClaims(roles);
                }
                */

                await HttpContext.SignInAsync(
                    IdentityServer4.IdentityServerConstants.ExternalCookieAuthenticationScheme,
                    new ClaimsPrincipal(id),
                    props).ConfigureAwait(false);
                return Redirect(props.RedirectUri);
            }
            else
            {
                // trigger windows auth
                // since windows auth don't support the redirect uri,
                // this URL is re-triggered when we call challenge
                return Challenge(AccountOptions.WindowsAuthenticationSchemeName);
            }
        }

        private async Task<(ApplicationUser user, string provider, string providerUserId, IEnumerable<Claim> claims)> FindUserFromExternalProvider(AuthenticateResult result)
        {
            var externalUser = result.Principal;

            // try to determine the unique id of the external user (issued by the provider)
            // the most common claim type for that are the sub claim and the NameIdentifier
            // depending on the external provider, some other claim type might be used
            var userIdClaim = externalUser.FindFirst(JwtClaimTypes.Subject) ??
                              externalUser.FindFirst(ClaimTypes.NameIdentifier) ??
                              throw new Exception("Unknown userid");

            // remove the user id claim so we don't include it as an extra claim if/when we provision the user
            var claims = externalUser.Claims.ToList();
            claims.Remove(userIdClaim);                             

            var provider = result.Properties.Items["scheme"];
            var providerUserId = userIdClaim.Value;

            // find external user            
            var user = await _userManager.FindByLoginAsync(provider, providerUserId).ConfigureAwait(false);

            return (user, provider, providerUserId, claims);
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "Template parameter")]
        private async Task<ApplicationUser> AutoProvisionUser(string provider, string providerUserId, IEnumerable<Claim> claims)
        {            
            // create a list of claims that we want to transfer into our store
            var filtered = new List<Claim>();
            #region Copy claims from external provider
            /*
            #region Add user's display name, GivenName, FamilyName claims
            var first = claims.FirstOrDefault(x => x.Type == JwtClaimTypes.GivenName)?.Value ??
                claims.FirstOrDefault(x => x.Type == ClaimTypes.GivenName)?.Value;
            var last = claims.FirstOrDefault(x => x.Type == JwtClaimTypes.FamilyName)?.Value ??
                claims.FirstOrDefault(x => x.Type == ClaimTypes.Surname)?.Value;
            var name = claims.FirstOrDefault(x => x.Type == JwtClaimTypes.Name)?.Value ??
                claims.FirstOrDefault(x => x.Type == ClaimTypes.Name)?.Value;

            if (name != null)
            {
                filtered.Add(new Claim(JwtClaimTypes.Name, name));
            }
            else
            {
                if (first != null && last != null)
                {
                    filtered.Add(new Claim(JwtClaimTypes.Name, first + " " + last));
                }
                else if (first != null)
                {
                    filtered.Add(new Claim(JwtClaimTypes.Name, first));
                }
                else if (last != null)
                {
                    filtered.Add(new Claim(JwtClaimTypes.Name, last));
                }
            }

            if (first != null)
            {
                filtered.Add(new Claim(JwtClaimTypes.GivenName, first));
            }
            if (last != null)
            {
                filtered.Add(new Claim(JwtClaimTypes.FamilyName, last));
            }
            #endregion
            */

            #region Add email claim
            // Use email as reference purpose
            var email = claims.FirstOrDefault(x => x.Type == JwtClaimTypes.Email)?.Value ??
               claims.FirstOrDefault(x => x.Type == ClaimTypes.Email)?.Value;
            if (email != null)
            {
                filtered.Add(new Claim(JwtClaimTypes.Email, email));
            }
            #endregion
            
            #endregion


            #region Add CLAIM_TYPE_SYSTEM_ROLE claim 
            // TODO:  Can we associate B4T user by using email + First name + last name and get userId here?
            // How to confirm it is the right user here(extra display step for user association?) 

            //filtered.Add(new Claim(B4TClaim.ClaimTypeUserId, "1234567890"));
            #endregion


            var user = new ApplicationUser
            {
                UserName = $"{provider}_{providerUserId}",
                LockoutEnabled = true,

                // If new user account default is lockout until verification, enable this.
                //LockoutEnd = DateTimeOffset.MaxValue
            };

            var identityResult = await _userManager.CreateAsync(user).ConfigureAwait(false);
            if (!identityResult.Succeeded)
            {
                _logger.LogError("Failed to create new local user for external user. Err: " + identityResult.Errors.First().Description);
                throw new Exception(identityResult.Errors.First().Description);
            }

            if (filtered.Any())
            {
                identityResult = await _userManager.AddClaimsAsync(user, filtered).ConfigureAwait(false);
                if (!identityResult.Succeeded) 
                {
                    _logger.LogError("Failed to add claims for external user. Err: " + identityResult.Errors.First().Description);
                    throw new Exception(identityResult.Errors.First().Description); 
                }
            }

            identityResult = await _userManager.AddLoginAsync(user, new UserLoginInfo(provider, providerUserId, provider)).ConfigureAwait(false);
            if (!identityResult.Succeeded)
            {
                _logger.LogError("Failed to add login info for external user. Err: " + identityResult.Errors.First().Description);
                throw new Exception(identityResult.Errors.First().Description);
            }

            return user;
        }

        private static IEnumerable<Claim> ConvertToJwtClaims(IEnumerable<Claim> externalClaims)
        {
            // create a list of claims that we want to transfer into our store
            var jwtClaims = new List<Claim>();
            
            #region Add user's display name, GivenName, FamilyName claims
            var first = externalClaims.FirstOrDefault(x => x.Type == JwtClaimTypes.GivenName)?.Value ??
                externalClaims.FirstOrDefault(x => x.Type == ClaimTypes.GivenName)?.Value;
            var last = externalClaims.FirstOrDefault(x => x.Type == JwtClaimTypes.FamilyName)?.Value ??
                externalClaims.FirstOrDefault(x => x.Type == ClaimTypes.Surname)?.Value;
            var name = externalClaims.FirstOrDefault(x => x.Type == JwtClaimTypes.Name)?.Value ??
                externalClaims.FirstOrDefault(x => x.Type == ClaimTypes.Name)?.Value;

            if (name != null)
            {
                jwtClaims.Add(new Claim(JwtClaimTypes.Name, name));
            }
            else
            {
                if (first != null && last != null)
                {
                    jwtClaims.Add(new Claim(JwtClaimTypes.Name, first + " " + last));
                }
                else if (first != null)
                {
                    jwtClaims.Add(new Claim(JwtClaimTypes.Name, first));
                }
                else if (last != null)
                {
                    jwtClaims.Add(new Claim(JwtClaimTypes.Name, last));
                }
            }

            if (first != null)
            {
                jwtClaims.Add(new Claim(JwtClaimTypes.GivenName, first));
            }
            if (last != null)
            {
                jwtClaims.Add(new Claim(JwtClaimTypes.FamilyName, last));
            }
            #endregion


            #region Add email claim
            var email = externalClaims.FirstOrDefault(x => x.Type == JwtClaimTypes.Email)?.Value ??
               externalClaims.FirstOrDefault(x => x.Type == ClaimTypes.Email)?.Value;
            if (email != null)
            {
                jwtClaims.Add(new Claim(JwtClaimTypes.Email, email));
            }
            #endregion

            return jwtClaims;

        }
        private static void ProcessLoginCallbackForOidc(AuthenticateResult externalResult, List<Claim> localClaims, AuthenticationProperties localSignInProps)
        {
            // if the external system sent a session id claim, copy it over
            // so we can use it for single sign-out
            var sid = externalResult.Principal.Claims.FirstOrDefault(x => x.Type == JwtClaimTypes.SessionId);
            if (sid != null)
            {
                localClaims.Add(new Claim(JwtClaimTypes.SessionId, sid.Value));
            }

            // if the external provider issued an id_token, we'll keep it for signout
            var id_token = externalResult.Properties.GetTokenValue("id_token");
            if (id_token != null)
            {
                localSignInProps.StoreTokens(new[] { new AuthenticationToken { Name = "id_token", Value = id_token } });
            }
        }

        /* Not use template method
        private void ProcessLoginCallbackForWsFed(AuthenticateResult externalResult, List<Claim> localClaims, AuthenticationProperties localSignInProps)
        {
        }

        private void ProcessLoginCallbackForSaml2p(AuthenticateResult externalResult, List<Claim> localClaims, AuthenticationProperties localSignInProps)
        {
        }
        */
    }
}
