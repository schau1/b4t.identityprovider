﻿using System;
using System.Linq;
using System.Security.Claims;
using IdentityModel;
using B4T.IdentityProvider.Data;
using B4T.IdentityProvider.Models;
using IdentityServer4.EntityFramework.DbContexts;
using IdentityServer4.EntityFramework.Storage;
using IdentityServer4.EntityFramework.Mappers;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Serilog;
using B4T.IdentityProvider.Constants;

namespace B4T.IdentityProvider
{
    public static class SeedData
    {
        public static void EnsureSeedData(string connectionString)
        {
            var services = new ServiceCollection();

            services.AddLogging();

            //Operation and config data
            services.AddOperationalDbContext(options =>
            {
                options.ConfigureDbContext = db => db.UseSqlServer(connectionString,
                    sql => sql.MigrationsAssembly(typeof(SeedData).Assembly.FullName));
            });
            services.AddConfigurationDbContext(options =>
            {
                options.ConfigureDbContext = db => db.UseSqlServer(connectionString,
                    sql => sql.MigrationsAssembly(typeof(SeedData).Assembly.FullName));
            });
            //Operation and config data

            // Asp.net Identity 
            services.AddDbContext<ApplicationDbContext>(options =>
               options.UseSqlServer(connectionString));
            // Asp.net Identity 

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();


            using (var serviceProvider = services.BuildServiceProvider())
            {
                using (var scope = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope())
                {
                    // server Configuration(resource / client) and Operational (tokens, codes, consents) Data
                    scope.ServiceProvider.GetService<PersistedGrantDbContext>().Database.Migrate();

                    var configContext = scope.ServiceProvider.GetService<ConfigurationDbContext>();
                    configContext.Database.Migrate();

                    EnsureSeedResourcesClientData(configContext);
                    // server Configuration( resource / client) and Operational (tokens, codes, consents) Data


                    // Asp.net Identity 
                    var context = scope.ServiceProvider.GetService<ApplicationDbContext>();
                    context.Database.Migrate();

                    var userMgr = scope.ServiceProvider.GetRequiredService<UserManager<ApplicationUser>>();
                    EnsureSeedUserData(userMgr);
                    // Asp.net Identity
                }
            }            
        }


        [System.Diagnostics.CodeAnalysis.SuppressMessage("Globalization", "CA1307:Specify StringComparison", Justification = "EF have problem with StringComparison syntex")]
        private static void EnsureSeedResourcesClientData(ConfigurationDbContext context)
        {
            if (!context.Clients.Any())
            {
                Log.Debug("Clients being populated");
                foreach (var client in Config.Clients.ToList())
                {
                    context.Clients.Add(client.ToEntity());
                }
                context.SaveChanges();
            }
            else
            {
                Log.Debug("Check is all clients populated");                
                foreach (var client in Config.Clients.ToList())
                {
                    if (!context.Clients.Any(c => c.ClientId.Equals(client.ClientId)))
                    {
                        // client not in DB 
                        Log.Debug("New client added. " + client.ClientName);
                        context.Clients.Add(client.ToEntity());
                    }
                }

            }

            if (!context.IdentityResources.Any())
            {
                Log.Debug("IdentityResources being populated");
                foreach (var resource in Config.Ids.ToList())
                {
                    context.IdentityResources.Add(resource.ToEntity());
                }
                context.SaveChanges();
            }
            else
            {                
                Log.Debug("Check is all IdentityResources populated");
                foreach (var resource in Config.Ids.ToList())
                {
                    if (!context.IdentityResources.Any(i => i.Name.Equals(resource.Name)))
                    {
                        Log.Debug("New id Resource added. " + resource.Name);
                        context.IdentityResources.Add(resource.ToEntity());
                    }
                }
                context.SaveChanges();

            }

            if (!context.ApiResources.Any())
            {
                Log.Debug("ApiResources being populated");
                foreach (var resource in Config.Apis.ToList())
                {
                    context.ApiResources.Add(resource.ToEntity());
                }
                context.SaveChanges();
            }
            else
            {                
                Log.Debug("Check is all ApiResources populated");
                foreach (var resource in Config.Apis.ToList())
                {
                    if (!context.ApiResources.Any(i => i.Name.Equals(resource.Name)))
                    {
                        Log.Debug("New api Resource added. " + resource.Name);
                        context.ApiResources.Add(resource.ToEntity());
                    }

                }
                context.SaveChanges();
            }
        }


        private static void EnsureSeedUserData(UserManager<ApplicationUser> userMgr)
        {
            #region Alice n Bob
            var alice = userMgr.FindByNameAsync("alice").Result;

            if (alice == null)
            {
                alice = new ApplicationUser
                {
                    UserName = "alice"
                };
                var result = userMgr.CreateAsync(alice, "Pass123$").Result;
                if (!result.Succeeded)
                {
                    throw new Exception(result.Errors.First().Description);
                }

                result = userMgr.AddClaimsAsync(alice, new Claim[]{
                        new Claim(JwtClaimTypes.Subject, "70EB5D94-4609-466D-814A-49D2CF03E4FB"),
                        new Claim(JwtClaimTypes.Name, "Alice Smith"),
                        new Claim(JwtClaimTypes.GivenName, "Alice"),
                        new Claim(JwtClaimTypes.FamilyName, "Smith"),
                        new Claim(JwtClaimTypes.Email, "AliceSmith@claimemail.com"),
                        new Claim(JwtClaimTypes.EmailVerified, "true", ClaimValueTypes.Boolean),
                        new Claim(JwtClaimTypes.WebSite, "http://alice.com"),
                        new Claim(JwtClaimTypes.Address, @"{ 'street_address': 'One Hacker Way', 'locality': 'Heidelberg', 'postal_code': 69118, 'country': 'Germany' }", IdentityServer4.IdentityServerConstants.ClaimValueTypes.Json),
                        // Give it a random userId
                        new Claim(B4TClaim.ClaimTypeUserId, "1111111")
                    }).Result;
                if (!result.Succeeded)
                {
                    throw new Exception(result.Errors.First().Description);
                }
                Log.Debug("alice created");
            }
            else
            {
                Log.Debug("alice already exists");
            }

            var bob = userMgr.FindByNameAsync("bob").Result;
            if (bob == null)
            {
                bob = new ApplicationUser
                {
                    UserName = "bob"
                };
                var result = userMgr.CreateAsync(bob, "Pass123$").Result;
                if (!result.Succeeded)
                {
                    throw new Exception(result.Errors.First().Description);
                }

                result = userMgr.AddClaimsAsync(bob, new Claim[]{
                        new Claim(JwtClaimTypes.Subject, "5DDFB032-8F4C-4F74-9CF3-A9A6CD9CF4F5"),
                        new Claim(JwtClaimTypes.Name, "Bob Smith"),
                        new Claim(JwtClaimTypes.GivenName, "Bob"),
                        new Claim(JwtClaimTypes.FamilyName, "Smith"),
                        new Claim(JwtClaimTypes.Email, "BobSmith@claimemail.com"),
                        new Claim(JwtClaimTypes.EmailVerified, "true", ClaimValueTypes.Boolean),
                        new Claim(JwtClaimTypes.WebSite, "http://bob.com"),
                        new Claim(JwtClaimTypes.Address, @"{ 'street_address': 'One Hacker Way', 'locality': 'Heidelberg', 'postal_code': 69118, 'country': 'Germany' }", IdentityServer4.IdentityServerConstants.ClaimValueTypes.Json),
                        new Claim(JwtClaimTypes.Gender, "M"),
                         // Give it a random userId
                        new Claim(B4TClaim.ClaimTypeUserId, "2222222")
                    }).Result;
                if (!result.Succeeded)
                {
                    throw new Exception(result.Errors.First().Description);
                }
                Log.Debug("bob created");
            }
            else
            {
                Log.Debug("bob already exists");
            }
            #endregion


            #region Identity Provider Admin
            var idadmin = userMgr.FindByNameAsync("idadmin").Result;
            if (idadmin == null)
            {
                idadmin = new ApplicationUser
                {
                    UserName = "idadmin"
                };

                var result = userMgr.CreateAsync(idadmin, "Admin123$").Result;
                if (!result.Succeeded)
                {
                    throw new Exception(result.Errors.First().Description);
                }

                // do not allow lockout of the id admin account.                  
                idadmin.LockoutEnabled = false;
                result = userMgr.UpdateAsync(idadmin).Result;
                if (!result.Succeeded)
                {
                    throw new Exception(result.Errors.First().Description);
                }
            

                result = userMgr.AddClaimsAsync(idadmin, new Claim[]{
                            new Claim(JwtClaimTypes.Subject, "38349917-FF20-4EA8-B743-C64869AEA3EE"),
                            new Claim(JwtClaimTypes.Name, "Identity Admin"),
                            new Claim(JwtClaimTypes.GivenName, "Identity"),
                            new Claim(JwtClaimTypes.FamilyName, "Admin"),
                            // Give it a random userId
                            new Claim(B4TClaim.ClaimTypeUserId, "999999999")
                        }).Result;
                if (!result.Succeeded)
                {
                    throw new Exception(result.Errors.First().Description);
                }
                Log.Debug("Identity Admin created");
            }
            else
            {
                Log.Debug("Identity Admin exists");
            }

            #endregion
        }
    }
}
