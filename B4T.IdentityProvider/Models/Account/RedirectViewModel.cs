// Copyright (c) Brock Allen & Dominick Baier. All rights reserved.
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.



namespace B4T.IdentityProvider.Models
{
    public class RedirectViewModel
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1056:Uri properties should not be strings", Justification = "Path string expected")]
        public string RedirectUrl { get; set; }
    }
}