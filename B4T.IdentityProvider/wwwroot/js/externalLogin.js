﻿const MDCTextfield = mdc.textField.MDCTextField;
//const MDCTextfieldFoundation = mdc.textField.MDCTextFieldFoundation;

const MDCSwitch = mdc.switchControl.MDCSwitch;
//const MDCSwitchFoundation = mdc.switchControl.MDCSwitchFoundation;



const foos_text = [].map.call(document.querySelectorAll('.mdc-text-field'), function (el) {
    return new MDCTextfield(el);
});

const foos_switch = [].map.call(document.querySelectorAll('.mdc-switch'), function (el) {
    return new MDCSwitch(el);
});


function showExternalProvider() {        
    var key = document.getElementById('pwd').value;
    if (key === 'b4T') {
        document.getElementById('externalProviders').style.display = 'block';
    } else {
        document.getElementById('externalProviders').style.display = 'none';        
    }        
}

function enableSignInButton() {
    var key = document.getElementById('pwd').value;    
    if (key === '') {
        document.getElementById('loginButton').disabled = true;
    } else {
        document.getElementById('loginButton').disabled = false;
    }        

}

document.getElementById('loginLogo').addEventListener('click', showExternalProvider);
document.getElementById('pwd').addEventListener('keyup', enableSignInButton);