﻿using IdentityModel;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;


namespace B4T.IdentityProvider.Extensions
{
    public static class ClaimsIdentityExtensions
    {
        static string GetUserEmail(this ClaimsIdentity identity)
        {
            return identity.Claims?.FirstOrDefault(c => c.Type == "Email")?.Value;
        }

        public static string GetUserEmail(this IIdentity identity)
        {
            return identity is ClaimsIdentity claimsIdentity ? GetUserEmail(claimsIdentity) : "";
        }

        static string GetUserNameIdentifier(this ClaimsIdentity identity)
        {
            var first = identity?.Claims.FirstOrDefault(x => x.Type == JwtClaimTypes.GivenName)?.Value ??
                identity?.Claims.FirstOrDefault(x => x.Type == ClaimTypes.GivenName)?.Value;
            var last = identity?.Claims.FirstOrDefault(x => x.Type == JwtClaimTypes.FamilyName)?.Value ??
                identity?.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Surname)?.Value;

            return first + " " + last;
        }

        public static string GetUserNameIdentifier(this IIdentity identity)
        {
            return identity is ClaimsIdentity claimsIdentity ? GetUserNameIdentifier(claimsIdentity) : "";
        }


    }
}
