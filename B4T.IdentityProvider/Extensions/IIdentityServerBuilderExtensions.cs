﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.IO;
using System.Security.Cryptography.X509Certificates;


namespace B4T.IdentityProvider.Extensions
{
    public static class IIdentityServerBuilderExtensions
    {
        /// <summary>
        ///  Add signing key for Identity server 4 based on config setting.
        ///  Expecting configuration in this format.
        ///    "SigningCredentials": {
        ///         "SigningType": ["X509"|"Developer"],
        ///         "KeyFile": "[KEYFILE].pfx",  // pfx file must be in the root path if used.  Use blank "" for Developer signing
        ///         "KeyFilePwd": "[KEYFILE_PASSWORD]"  //Use blank "" for Developer signing
        ///     }
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="hostEnv"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        public static IIdentityServerBuilder AddSignCredential(
            this IIdentityServerBuilder builder, IWebHostEnvironment hostEnv, IConfigurationSection options)
        {
            _ = hostEnv ?? throw new ArgumentNullException(nameof(hostEnv));

            string signingType = options.GetValue<string>("SigningType");


            switch (signingType)
            {
                case "CertStore":
                    AddX509CertFromStore(builder, hostEnv, options);
                    break;

                case "X509File":
                    AddX509CertFromFile(builder, hostEnv, options);
                    break;

                case "Developer":
                    builder.AddDeveloperSigningCredential();
                    break;

                default:
                    throw new Exception("Need to configure signing key material");
            }

            return builder;
        }

        private static void AddX509CertFromFile(IIdentityServerBuilder builder,
            IWebHostEnvironment hostEnv, IConfigurationSection options)
        {            
            // Get from file
            var keyFile = options.GetValue<string>("KeyFile");
            var keyFilePassword = options.GetValue<string>("KeyFilePwd");

            var fullKeyFilePath = Path.Combine(hostEnv.ContentRootPath, keyFile);
            if (File.Exists(fullKeyFilePath))
            {
                /*
                 * Note:  File cert depends on private key from store when using X509Certificate2(CertPath, ...)
                 * This is a known issue https://github.com/aspnet/AspNetCore/issues/3218.
                 * Do not use file cert path interface until it is fixed. (After Core 2.2.0)
                 * !!! Even fix it might not work in both Windowns and Linux environment.
                 */
                //cert = new X509Certificate2(fullKeyFilePath, keyFilePassword, X509KeyStorageFlags.MachineKeySet);

                // Read bytes from file first and pass it to X509Cert.  This work for both Linux and Windows.
                using (X509Certificate2 cert = new X509Certificate2(File.ReadAllBytes(fullKeyFilePath), keyFilePassword, X509KeyStorageFlags.MachineKeySet))
                {
                    builder.AddSigningCredential(cert);
                }
            }
            else
            {
                throw new Exception($"SigningCredentialExtension cannot find key file {fullKeyFilePath}");
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Usage", "CA1801:Review unused parameters", Justification = "Template parameter")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "Template parameter")]
        private static void AddX509CertFromStore(IIdentityServerBuilder builder,
            IWebHostEnvironment hostEnv, IConfigurationSection options)
        {
            // For Window Nano Only
            X509Store store = new X509Store(StoreName.My, StoreLocation.LocalMachine);
            try
            {
                store.Open(OpenFlags.ReadOnly);

                var certThumbprint = options.GetValue<string>("CertThumbprint");

                var certificates = store.Certificates.Find(X509FindType.FindByThumbprint, certThumbprint, false);

                if (certificates.Count > 0)
                {
                    builder.AddSigningCredential(certificates[0]);
                }
                else
                {
                    throw new Exception($"A matching key couldn't be found in the store with thumbprint {certThumbprint}");
                }
            }
            finally
            {
                store.Close();
            }
        }


    }
}
