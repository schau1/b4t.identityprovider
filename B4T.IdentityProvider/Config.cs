﻿using B4T.IdentityProvider.Constants;
using IdentityServer4;
using IdentityServer4.Models;
using System.Collections.Generic;


namespace B4T.IdentityProvider
{
    public static class Config
    {
        public static IEnumerable<IdentityResource> Ids =>
            new IdentityResource[]
            {
                new IdentityResources.OpenId(),     // subject id
                new IdentityResources.Profile(),    // Add using standardscope "profile" 
                // which map to IdentityModel.JwtClaimTypes Name, FamilyName, GivenName, MiddleName, NickName, PreferredUserName, Profile, Picture, WebSite, Gender, BirthDate, ZoneInfo, Locale, UpdateAt

                // user identity resources can be request by Client.
                new IdentityResources.Email(),  // JwtClaimTypes Email, EmailVerified
                
                new IdentityResources.Address(),
                new IdentityResources.Phone(),

                #region scope, display name, list of claim types related to the scope to return
                // claim types required to support B4T profile scope
                new IdentityResource("b4tprofile", "B4T Profile", new List<string>{ B4TClaim.ClaimTypeUserId, B4TClaim.ClaimTypeUserActive }),
                #endregion
            };


        public static IEnumerable<ApiResource> Apis =>
            new ApiResource[]
            {
                #region Online Booking API
                new ApiResource("obxapi_q", "Online Booking API (QA)"),
                new ApiResource("obxapi_r", "Online Booking API (Regression)"),
                new ApiResource("obxapi", "Online Booking API"),
                #endregion

                new ApiResource("api1", "IdpSvr Test API")
            };


        public static IEnumerable<Client> Clients =>
           new Client[]
            {

                #region Online Booking SPA Client
                // Dev/QA
                new Client
                {
                    ClientId = "obxClient_q",
                    ClientName = "Online Booking SPA Client QA",
                    AllowedGrantTypes = GrantTypes.Code,
                    AlwaysIncludeUserClaimsInIdToken = true,
                    RequireConsent = false,
                    RequirePkce = true,
                    RequireClientSecret = false,

                    RedirectUris =           { "https://localhost:5005/authentication/login-callback", "https://obxq.book4time.com/authentication/login-callback" },
                    PostLogoutRedirectUris = { "https://localhost:5005/authentication/logout-callback", "https://obxq.book4time.com/authentication/logout-callback"},
                    AllowedCorsOrigins =     { "https://localhost:5005", "https://obxq.book4time.com" },

                    AllowedScopes =
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        "b4tprofile",
                        "obxapi_q"
                    }
                },

                //Regression
                new Client
                {
                    ClientId = "obxClient_r",
                    ClientName = "Online Booking SPA Client Regression",
                    AllowedGrantTypes = GrantTypes.Code,
                    AlwaysIncludeUserClaimsInIdToken = true,
                    RequireConsent = false,
                    RequirePkce = true,
                    RequireClientSecret = false,

                    RedirectUris =           { "https://obxr.book4time.com/authentication/login-callback" },
                    PostLogoutRedirectUris = { "https://obxr.book4time.com/authentication/logout-callback"},
                    AllowedCorsOrigins =     { "https://obxr.book4time.com" },

                    AllowedScopes =
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        "b4tprofile",
                        "obxapi_r"
                    }
                },

                //Production
                new Client
                {
                    ClientId = "obxClient",
                    ClientName = "Online Booking SPA Client",
                    AllowedGrantTypes = GrantTypes.Code,
                    AlwaysIncludeUserClaimsInIdToken = true,
                    RequireConsent = false,
                    RequirePkce = true,
                    RequireClientSecret = false,

                    RedirectUris =           { "https://obx.book4time.com/authentication/login-callback" },
                    PostLogoutRedirectUris = { "https://obx.book4time.com/authentication/logout-callback"},
                    AllowedCorsOrigins =     { "https://obx.book4time.com" },

                    AllowedScopes =
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        "b4tprofile",
                        "obxapi"
                    }
                }
                #endregion
            };


    }
}