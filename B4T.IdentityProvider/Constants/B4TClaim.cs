﻿namespace B4T.IdentityProvider.Constants
{
    public static class B4TClaim
    {
        // Claim types and values
        public const string ClaimTypeUserActive = "b4t_user_active";
        public const string ClaimTypeUserId = "b4t_user_id";

    }
}
